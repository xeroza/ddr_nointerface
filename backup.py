
#evdev takes care of polling the controller in a loop
for event in gamepad.read_loop():
    #filters by event type
    if event.type == ecodes.EV_KEY:
        if event.code == Button.up.code:
            if event.value:
                Button.up.pressed = True
            else:
                Button.up.pressed = False
            print('up' + str(Button.up.pressed))
            hitSound()
        if event.code == Button.down.code:
            if event.value:
                Button.down.pressed = True
            else:
                Button.down.pressed = False
            print('down' + str(Button.down.pressed))
        if event.code == Button.left.code:
            if event.value:
                Button.left.pressed = True
            else:
                Button.left.pressed = False
            print('left' + str(Button.left.pressed))
        if event.code == Button.right.code:
            if event.value:
                Button.right.pressed = True
            else:
                Button.right.pressed = False
            print('right' + str(Button.right.pressed))
        if event.code == Button.x.code:
            if event.value:
                Button.x.pressed = True
            else:
                Button.x.pressed = False
            print('x' + str(Button.x.pressed))
        if event.code == Button.circle.code:
            if event.value:
                Button.circle.pressed = True
            else:
                Button.circle.pressed = False
            print('circle' + str(Button.circle.pressed))
        if event.code == Button.triangle.code:
            if event.value:
                Button.triangle.pressed = True
            else:
                Button.triangle.pressed = False
            print('trianngle' + str(Button.triangle.pressed))
        if event.code == Button.square.code:
            if event.value:
                Button.square.pressed = True
            else:
                Button.square.pressed = False
            print('square' + str(Button.square.pressed))
        if event.code == Button.select.code:
            if event.value:
                Button.select.pressed = True
            else:
                Button.select.pressed = False
            print('select' + str(Button.select.pressed))
        if event.code == Button.start.code:
            if event.value:
                Button.start.pressed = True
            else:
                Button.start.pressed = False
            print('start' + str(Button.start.pressed))